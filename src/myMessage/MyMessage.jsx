import React from "react";
import '../spinner/Spinner.css'
import './MyMessage.css'

class MyMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showEditMessage: false
        }

        this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
        this.handleEditMessage = this.handleEditMessage.bind(this);
    }

    render() {
        return (
            <>
                {this.props.data.divider}
                <div className='message-row'>
                    <div className='user-menu'>
                        <a onClick={this.handleEditMessage} href="/#"><i className="edit icon"/></a>
                        <a onClick={this.handleDeleteMessage} className='trash' href="/#"><i
                            className="trash alternate icon"/></a>
                    </div>
                    <div className='message-wrp'>
                        <div className='message'>
                            <div className='header-message'>
                                <span>{this.props.data.message.user}</span>
                                <span>{this.getDateTimeMessage()}</span>
                            </div>
                            <div className='text-message'>
                                <span>{this.props.data.message.text}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }

    getDateTimeMessage() {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit"
        }).format(new Date(this.props.data.message.createdAt).getTime());
    }

    handleDeleteMessage() {
        const uuid = this.props.data.message.id;
        this.props.deleteMessage(uuid);
    }

    handleEditMessage() {
        const message = this.props.data.message;
        this.props.editMessage(message);
    }
}

export default MyMessage
