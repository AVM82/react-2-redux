import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css'

import Chat from './chat/Chat';
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import rootReducers from "./rootReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

const store = createStore(rootReducers, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Chat/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

