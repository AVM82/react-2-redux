export const getUniqueUser = (mes) => {
    return mes.reduce(function (a, d) {
        if (a.indexOf(d.userId) === -1) {
            a.push(d.userId);
        }
        return a;
    }, []);
}

export const convertDate = (date) => {
    return new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "long",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit"
    }).format(date);
}

export const convertToOnlyDate = (date) => {
    return new Intl.DateTimeFormat("en-GB", {
        year: "numeric",
        month: "long",
        day: "2-digit",
    }).format(date);
}

export const create_UUID = () => {
    let dt = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c === 'x' ? r : (r && 0x3 | 0x8)).toString(16);
    });
}


export default {getUniqueUser, convertDate, convertToOnlyDate, create_UUID}