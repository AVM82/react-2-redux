import React from "react";
import './SendMessage.css'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addMessage} from "../chat/action";

class SendMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    render() {
        return (
            <div className='message-input-wrp'>
                <div className='message-input'>
                    <input
                        onChange={this.handleChange}
                        value={this.state.text}
                        placeholder="Type your message here"
                    />
                    <div className='send-button'>
                        <button onClick={this.handleSubmit} className="ui circular twitter icon button">
                            <i className="share icon"/>
                        </button>
                    </div>
                </div>
            </div>

        );
    }

    handleChange(event) {
        this.setState({
            text: event.target.value
        })
    }

    handleSubmit(event) {
        if (this.state.text === '') return
        event.preventDefault()
        this.props.addMessage(this.state.text)
        this.setState({
            text: ''
        })
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addMessage: addMessage
    }, dispatch)
}

export default connect(null, mapDispatchToProps)(SendMessage);