import {HIDE_PAGE, SAVE_MESSAGE, SHOW_PAGE} from "./actionType";


export const showPage = (message) => {
    const payload = {
        showEdit: true,
        message: message
    }

    return {
        type: SHOW_PAGE,
        payload: payload
    }
}

export const hidePage = () => {
    return {
        type: HIDE_PAGE,
        payload: false
    }
}

export const saveMessage = (messages) => {
    const payload = {
        showEdit: false,
        messages: messages
    }

    return {
        type: SAVE_MESSAGE,
        payload: payload
    }
}