import React from "react";
import './OtherMessage.css'

class OtherMessage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            like: false
        }

        this.handleLike = this.handleLike.bind(this);
    }

    render() {
        return (

            <>
                {this.props.data.divider}
                <div className='other-mes-row'>
                    <div className='message-wrp'>
                        <div className='avatar'>
                            <img className="ui medium circular image" src={this.props.data.message.avatar} alt=''/>
                            <img className="ui medium circular image" src='' alt=''/>
                        </div>
                        <div className='message'>
                            <div className='header-message'>
                                <span>{this.props.data.message.user}</span>
                                <span>{this.getDateTimeMessage()}</span>
                            </div>
                            <div className='text-message'>
                                <span>{this.props.data.message.text}</span>
                                <span>3214</span>
                            </div>
                        </div>

                    </div>
                    <div className='heart-wrp'>
                        {this.state.like ?
                            <a href="/#"><i onClick={this.handleLike} className="heart icon"/></a> :
                            <a href="/#"><i onClick={this.handleLike} className="heart outline icon"/></a>}
                    </div>
                </div>
            </>
        );
    }

    getDateTimeMessage() {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit"
        }).format(new Date(this.props.data.message.createdAt).getTime());
    }

    handleLike() {
        this.setState({
            like: !this.state.like
        })
    }

}

export default OtherMessage