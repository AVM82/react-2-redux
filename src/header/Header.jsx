import React from "react";
import './Header.css'

class Header extends React.Component {

    render() {

        return (
            <div className='header-wrp'>
                <div className='header-left'>
                    <a href="/#">My chat</a>
                    <a href="/#"><i className="user secret icon"/>{this.props.data.uniqueUser} participants</a>
                    <a href="/#"><i className="wechat icon"/>{this.props.data.countMes} messages</a>

                </div>
                <div>
                    <a href="/#">last message at: {this.props.data.lastMessage}</a>
                </div>
            </div>
        )
    }
}

export default Header;