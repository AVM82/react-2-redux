import {ADD_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGE} from "./actionType";
import {HIDE_PAGE, SAVE_MESSAGE, SHOW_PAGE} from "../editMessage/actionType";

const initialState = {
    loading: true,
    myId: 333,
    messages: [],
    lastMessage: '',
    uniqueUser: 0,
    showEdit: false,
    editMessage: {}

};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE: {
            return {
                ...state,
                uniqueUser: action.payload.uniqueUser,
                lastMessage: action.payload.lastMessage,
                messages: action.payload.messages,
                loading: false
            };

        }

        case ADD_MESSAGE: {
            return {
                ...state,
                messages: [...state.messages, action.payload],
                lastMessage: action.payload.createdAt
            };

        }
        case SHOW_PAGE: {


            return {
                ...state,
                showEdit: action.payload.showEdit,
                editMessage: action.payload.message
            };

        }

        case HIDE_PAGE: {
            return {
                ...state,
                showEdit: action.payload
            };

        }

        case SAVE_MESSAGE: {
            return {
                ...state,
                showEdit: action.payload.showEdit,
                messages: action.payload.messages
            };

        }

        case DELETE_MESSAGE: {
            return {
                ...state,
                messages: action.payload
            };

        }

        default:
            return state;
    }
}
