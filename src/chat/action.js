import {ADD_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGE} from "./actionType";
import {convertDate, create_UUID, getUniqueUser} from "../service/service";


export const getMessage = () => dispatch => {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then((res) => res.json())
        .then((res) => {
            return res.sort((a, b) => {
                const first = new Date(a.createdAt).getTime();
                const second = new Date(b.createdAt).getTime();
                if (first > second) {
                    return 1;
                } else if (first < second) {
                    return -1;
                }
                return 0;
            })
        })
        .then((res) => {
                const count = Object.keys(res).length;
                const uniqueUser = getUniqueUser(res);
                const lastMesDateTime = convertDate(new Date(res[count - 1].createdAt).getTime())
                dispatch({
                    type: FETCH_MESSAGE,
                    payload: {
                        messages: res,
                        uniqueUser: uniqueUser.length,
                        lastMessage: lastMesDateTime
                    },
                })
            }
        )
        .catch((error) => {
            console.log(error);
        });
}

export const addMessage = (text) => {

    const date = new Date();
    const mes = {
        id: create_UUID(),
        userId: 333,
        avatar: "",
        user: "Me",
        "text": text,
        "createdAt": convertDate(date),
        "editedAt": ""
    }
    return {
        type: ADD_MESSAGE,
        payload: mes
    }
}

export const deleteMessage = (messages, uuid) => {

    const array = Array.from(messages);
    const deleteIndex = array.map(function (item) {
        return item.id;
    }).indexOf(uuid);
    array.splice(deleteIndex, 1);

    return {
        type: DELETE_MESSAGE,
        payload: array
    }

}
