import React from 'react';
import './Chat.css';
import Header from "../header/Header";
import Spinner from "../spinner/Spinner";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteMessage, getMessage} from "./action";
import MyMessage from "../myMessage/MyMessage";
import OtherMessage from "../otherMessage/OtherMessage";
import {convertToOnlyDate} from "../service/service";
import SendMessage from "../sendMessage/SendMessage";
import EditMessage from "../editMessage/EditMessage";
import {showPage} from "../editMessage/action";


class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.editMessage = this.editMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);

    }


    componentDidMount() {
        this.props.getMessage();
    }

    editMessage(message) {
        this.props.showPage(message);
    }

    deleteMessage(uuid) {
        this.props.deleteMessage(this.props.messages, uuid);
    }

    render() {
        if (this.props.loading) return <Spinner/>;
        let messageDate = undefined;
        return (
            <>
                <div className='chat-wrp'>
                    <EditMessage/>
                    <Header data={
                        {
                            uniqueUser: this.props.uniqueUser + 1,
                            countMes: this.props.messages.length,
                            lastMessage: this.props.lastMessageDateTime
                        }
                    }/>

                    <div className='main-wrp'>
                        <div className='message-list-wrp'>
                            {
                                this.props.messages.map((item, index) => {
                                    let divider;
                                    let currentMesDate = convertToOnlyDate(new Date(item.createdAt).getTime())
                                    if (messageDate !== currentMesDate) {
                                        messageDate = currentMesDate;
                                        divider = <div className="ui horizontal divider">{currentMesDate}</div>
                                    } else {
                                        divider = null;
                                    }
                                    return (
                                        (item.userId === 333) ?
                                            <MyMessage data={{
                                                message: item,
                                                divider: divider
                                            }}
                                                       editMessage={this.editMessage}
                                                       deleteMessage={this.deleteMessage}
                                                       key={index}/> :
                                            <OtherMessage data={{
                                                message: item,
                                                divider: divider
                                            }}
                                                          key={index}/>)
                                })
                            }
                            <div className='hack-padding'
                                 ref={(el) => {
                                     this.messagesEnd = el;
                                 }}>
                            </div>
                        </div>
                    </div>
                    <SendMessage/>
                </div>
            </>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getMessage: getMessage,
        showPage: showPage,
        deleteMessage: deleteMessage
    }, dispatch)

}

function mapStateToProps(state) {
    return {
        messages: state.ChatReducer.messages,
        loading: state.ChatReducer.loading,
        uniqueUser: state.ChatReducer.uniqueUser,
        lastMessageDateTime: state.ChatReducer.lastMessage,
        myId: state.ChatReducer.myId,

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
